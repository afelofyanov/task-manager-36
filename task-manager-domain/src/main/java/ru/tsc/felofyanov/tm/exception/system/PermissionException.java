package ru.tsc.felofyanov.tm.exception.system;

import ru.tsc.felofyanov.tm.exception.AbstractExcception;

public final class PermissionException extends AbstractExcception {
    public PermissionException() {
        super("ERROR! Permission is incorrect....");
    }
}
