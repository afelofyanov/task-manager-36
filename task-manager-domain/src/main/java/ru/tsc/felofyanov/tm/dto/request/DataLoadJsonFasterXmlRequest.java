package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadJsonFasterXmlRequest extends AbstractUserRequest {

    public DataLoadJsonFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
