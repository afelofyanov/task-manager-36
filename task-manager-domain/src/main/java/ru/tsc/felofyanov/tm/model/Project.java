package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.model.IWBS;
import ru.tsc.felofyanov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractUserOwnerModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public Project(@NotNull final String name, @NotNull final Status status, @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + " (" + getId() + ") : " + getStatus().getDisplayName();
    }
}
