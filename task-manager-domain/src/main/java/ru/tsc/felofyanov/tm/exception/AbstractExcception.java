package ru.tsc.felofyanov.tm.exception;

public abstract class AbstractExcception extends RuntimeException {

    public AbstractExcception() {
    }

    public AbstractExcception(String message) {
        super(message);
    }

    public AbstractExcception(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractExcception(Throwable cause) {
        super(cause);
    }

    public AbstractExcception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
