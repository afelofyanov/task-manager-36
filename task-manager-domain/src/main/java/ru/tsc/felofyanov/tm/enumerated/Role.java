package ru.tsc.felofyanov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Role {
    ADMIN("Administration"),
    USUAL("Usual user");

    @Getter
    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }
}
