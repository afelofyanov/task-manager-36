package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveXmlFasterXmlRequest extends AbstractUserRequest {

    public DataSaveXmlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
