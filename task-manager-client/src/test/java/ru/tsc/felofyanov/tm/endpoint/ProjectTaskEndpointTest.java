package ru.tsc.felofyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;

public class ProjectTaskEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IProjectTaskEndpoint endpoint = IProjectTaskEndpoint.newInstance();

    @Nullable
    private String token;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse =
                authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(new ProjectRemoveByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(
                new ProjectRemoveByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(
                new ProjectRemoveByIdRequest("123", null)
        ));

        @Nullable ProjectCreateResponse createResponse =
                projectEndpoint.createProject(new ProjectCreateRequest(
                        token, "ProjectForRemove", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());

        @Nullable Project project = createResponse.getProject();
        Assert.assertNotNull(project);

        TaskCreateResponse taskId =
                taskEndpoint.createTask(new TaskCreateRequest(
                        token, "TaskForRemove", "test", null, null
                ));
        Assert.assertNotNull(taskId.getTask());
        @Nullable Task task = taskId.getTask();
        Assert.assertNotNull(task);

        ProjectBindTaskByIdResponse bindTask =
                endpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(token, project.getId(), task.getId()));
        Assert.assertNotNull(bindTask);

        ProjectRemoveByIdResponse response =
                endpoint.removeProjectById(new ProjectRemoveByIdRequest(token, project.getId()));
        Assert.assertNotNull(response);

        ProjectGetByIdResponse projectTest =
                projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, project.getId()));
        Assert.assertNotNull(projectTest);
        Assert.assertNull(projectTest.getProject());

        TaskGetByIdResponse taskTest = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, task.getId()));
        Assert.assertNotNull(taskTest);
        Assert.assertNull(taskTest.getTask());
    }

    @Test
    public void bindTaskToProjectId() {
        Assert.assertThrows(Exception.class, () -> endpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.bindTaskToProjectId(
                new ProjectBindTaskByIdRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.bindTaskToProjectId(
                new ProjectBindTaskByIdRequest("123", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.bindTaskToProjectId(
                new ProjectBindTaskByIdRequest(token, "123", null)
        ));

        ProjectGetByIndexResponse projectId =
                projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, 4));
        Assert.assertNotNull(projectId.getProject());

        @Nullable Project project = projectId.getProject();

        TaskCreateResponse taskId =
                taskEndpoint.createTask(new TaskCreateRequest(
                        token, "bindTaskToProjectId", "test", null, null
                ));
        Assert.assertNotNull(taskId.getTask());
        @Nullable Task task = taskId.getTask();

        ProjectBindTaskByIdResponse response =
                endpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(token, project.getId(), task.getId()));
        Assert.assertNotNull(response);

        TaskGetByIdResponse result = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, task.getId()));
        Assert.assertNotNull(result.getTask());
        Assert.assertEquals(result.getTask().getProjectId(), project.getId());
    }

    @Test
    public void unbindTaskFromProjectId() {
        Assert.assertThrows(Exception.class, () -> endpoint.unbindTaskFromProjectId(new ProjectUnbindTaskByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.unbindTaskFromProjectId(
                new ProjectUnbindTaskByIdRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.unbindTaskFromProjectId(
                new ProjectUnbindTaskByIdRequest("123", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.unbindTaskFromProjectId(
                new ProjectUnbindTaskByIdRequest(token, "123", null)
        ));

        ProjectGetByIndexResponse projectId =
                projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, 4));
        Assert.assertNotNull(projectId.getProject());

        @Nullable Project project = projectId.getProject();

        TaskGetByIndexResponse taskId = taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest(token, 2));
        Assert.assertNotNull(taskId.getTask());
        @Nullable Task task = taskId.getTask();

        ProjectBindTaskByIdResponse bindTask =
                endpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(token, project.getId(), task.getId()));
        Assert.assertNotNull(bindTask);

        TaskGetByIdResponse resultBefore = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, task.getId()));
        Assert.assertNotNull(resultBefore.getTask());
        Assert.assertNotNull(resultBefore.getTask().getProjectId());

        ProjectUnbindTaskByIdResponse unbindTask =
                endpoint.unbindTaskFromProjectId(new ProjectUnbindTaskByIdRequest(token, project.getId(), task.getId()));
        Assert.assertNotNull(unbindTask);

        TaskGetByIdResponse resultAfter = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, task.getId()));
        Assert.assertNotNull(resultAfter.getTask());
        Assert.assertNull(resultAfter.getTask().getProjectId());
    }
}
