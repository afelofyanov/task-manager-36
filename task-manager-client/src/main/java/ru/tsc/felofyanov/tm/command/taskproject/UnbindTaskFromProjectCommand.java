package ru.tsc.felofyanov.tm.command.taskproject;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.ProjectUnbindTaskByIdRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class UnbindTaskFromProjectCommand extends AbstractTaskProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "unbind-task-to-project";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final ProjectUnbindTaskByIdRequest request =
                new ProjectUnbindTaskByIdRequest(getToken(), projectId, taskId);
        getServiceLocator().getProjectTaskEndpoint().unbindTaskFromProjectId(request);
    }
}
