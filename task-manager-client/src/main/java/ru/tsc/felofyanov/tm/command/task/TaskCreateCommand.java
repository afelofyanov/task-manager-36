package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.TaskCreateRequest;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");

        System.out.println("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        System.out.println("ENTER DATE BEGIN: ");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();

        System.out.println("ENTER DATE END: ");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final TaskCreateRequest request =
                new TaskCreateRequest(getToken(), name, description, dateBegin, dateEnd);
        getServiceLocator().getTaskEndpoint().createTask(request);
    }
}
