package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    ITokenService getTokenService();

}
