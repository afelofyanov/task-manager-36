package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");

        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskChangeStatusByIdRequest request =
                new TaskChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        getServiceLocator().getTaskEndpoint().changeTaskStatusById(request);
    }
}
