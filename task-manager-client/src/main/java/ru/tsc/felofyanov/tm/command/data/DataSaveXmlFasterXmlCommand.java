package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.DataSaveXmlFasterXmlRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;

public final class DataSaveXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-fasterxml";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file(FasterXML)";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getDomainEndpoint().saveDataXmlFasterXml(new DataSaveXmlFasterXmlRequest(getToken()));
    }
}
