package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ProjectService projectService = new ProjectService(projectRepository);

    @Test
    public void add() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(null, null));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.add("123", null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add("", null));

        @NotNull Project project = new Project();
        projectService.add("123", project);
        Assert.assertFalse(projectService.findAll().isEmpty());

        Assert.assertNotNull(projectService.findAll(project.getUserId()));
    }

    @Test
    public void updateById() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, "test", "test", "test"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById("test", "test", null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById("test", "test", "test", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById("test", "test", "test", "test"));

        @NotNull Project project = projectService.create("test", "DEMKA", "test");
        Assert.assertFalse(projectService.findAll().isEmpty());

        Assert.assertNotNull(projectService.updateById("test1", project.getId(), "No demo", "tester"));
    }

    @Test
    public void updateByIndex() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(null, 0, "test", "test"));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex("test", 0, null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateByIndex("test", 0, "test", null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex("test", -1, "test", "test"));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> projectService.updateByIndex("test", 1, "test", "test"));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> projectService.updateByIndex("test", 0, "test", "test"));

        projectService.create("test", "DEMKA", "test");
        Assert.assertFalse(projectService.findAll().isEmpty());

        Assert.assertNotNull(projectService.updateByIndex("test1", 0, "No demo", "tester"));
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, "test", Status.NOT_STARTED));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeProjectStatusById("test", "test", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusById("test", "test", Status.NOT_STARTED));

        @NotNull Project project = projectService.create("test", "DEMKA", "test");
        Assert.assertFalse(projectService.findAll().isEmpty());

        Assert.assertNotNull(projectService.changeProjectStatusById("test1", project.getId(), Status.IN_PROGRESS));
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(null, 0, Status.NOT_STARTED));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeProjectStatusByIndex("test", 0, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex("test", -1, Status.NOT_STARTED));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> projectService.changeProjectStatusByIndex("test", 1, Status.NOT_STARTED));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> projectService.changeProjectStatusByIndex("test", 0, Status.NOT_STARTED));

        projectService.create("test", "DEMKA", "test");
        Assert.assertFalse(projectService.findAll().isEmpty());

        Assert.assertNotNull(projectService.changeProjectStatusByIndex("test1", 0, Status.IN_PROGRESS));
    }

    @Test
    public void create() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, "test"));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, null));

        Assert.assertThrows(NameEmptyException.class, () -> projectService.create("", null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create("test", null));

        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create("test", "test", null));

        projectService.create("test", "test");
        Assert.assertFalse(projectService.findAll().isEmpty());
        @Nullable List<Project> serviceTest = projectService.findAll("test1");
        Assert.assertEquals(0, serviceTest.size());

        serviceTest = projectService.findAll("test");
        Assert.assertEquals(1, serviceTest.size());
    }
}
