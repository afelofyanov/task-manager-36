package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    @NotNull
    private final TaskRepository repository = new TaskRepository();

    @Test
    public void add() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = new Task();
        task.setName("test1");
        task.setDescription("1111");
        task.setUserId("123456");
        repository.add(task);
        Assert.assertFalse(repository.findAll().isEmpty());
        Assert.assertNotNull(repository.findOneById(task.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.create("test", "DEMKA");
        Assert.assertFalse(repository.findAll(Sort.BY_NAME.getComparator()).isEmpty());
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create("test2", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.findOneById(""));
        Assert.assertNotNull(repository.findOneById(task.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));

        Assert.assertNull(repository.findOneById("", task.getId()));
        Assert.assertNull(repository.findOneById(null, task.getId()));
        Assert.assertNull(repository.findOneById(null, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(task.getUserId(), ""));
        Assert.assertNull(repository.findOneById(task.getUserId(), null));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), null));

        Assert.assertNotNull(repository.findOneById(task.getUserId(), task.getId()));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create("test2", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNotNull(repository.findOneByIndex(0));

        Assert.assertNull(repository.findOneByIndex(null, 0));
        Assert.assertNull(repository.findOneByIndex(task.getUserId(), null));
        Assert.assertNull(repository.findOneByIndex(UUID.randomUUID().toString(), null));

        Assert.assertNotNull(repository.findOneByIndex(task.getUserId(), 0));
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertTrue(repository.findAll().isEmpty());

        @NotNull final Task task = repository.create("test2", "DEMKA");
        task.setProjectId("1111");
        repository.add(task);

        @Nullable List<Task> testTask = repository.findAllByProjectId("test2", "12345");
        Assert.assertFalse(repository.findAll().isEmpty());
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, testTask.size());

        testTask = repository.findAllByProjectId(null, null);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, testTask.size());

        testTask = repository.findAllByProjectId(null, "123");
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, testTask.size());

        testTask = repository.findAllByProjectId(null, UUID.randomUUID().toString());
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, testTask.size());

        testTask = repository.findAllByProjectId("123", null);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, testTask.size());

        testTask = repository.findAllByProjectId(UUID.randomUUID().toString(), null);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, testTask.size());

        testTask = repository.findAllByProjectId(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, testTask.size());
    }

    @Test
    public void remove() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNotNull(repository.remove(task));

        Assert.assertNull(repository.remove(null));
        Assert.assertNull(repository.remove(null, null));
        Assert.assertNull(repository.remove(task.getUserId(), null));
        Assert.assertNull(repository.remove("12321", task));
        Assert.assertNull(repository.remove(UUID.randomUUID().toString(), task));
    }

    @Test
    public void clear() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(task.getUserId()));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));

        repository.clear();
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(0, repository.count(task.getUserId()));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
    }

    @Test
    public void removeById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        @Nullable Task testTask = repository.removeById(null);
        Assert.assertNull(testTask);
        Assert.assertEquals(1, repository.count());

        testTask = repository.removeById(null, null);
        Assert.assertNull(testTask);
        Assert.assertEquals(1, repository.count());

        testTask = repository.removeById(task.getUserId(), null);
        Assert.assertNull(testTask);
        Assert.assertEquals(1, repository.count());

        testTask = repository.removeById(task.getId());
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void removeByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        @Nullable Task testTask = repository.removeByIndex(null);
        Assert.assertNull(testTask);
        Assert.assertEquals(1, repository.count());

        testTask = repository.removeByIndex(null, null);
        Assert.assertNull(testTask);
        Assert.assertEquals(1, repository.count());

        testTask = repository.removeByIndex(task.getUserId(), null);
        Assert.assertNull(testTask);
        Assert.assertEquals(1, repository.count());

        testTask = repository.removeByIndex(0);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void create() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById("123"));

        Assert.assertNull(repository.findOneById(null, null));
        Assert.assertNull(repository.findOneById(null, task.getId()));
        Assert.assertNull(repository.findOneById("1233", null));

        Assert.assertNotNull(repository.findOneById(task.getId()));
        Assert.assertNotNull(repository.findOneById(task.getUserId(), task.getId()));
    }
}
