package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.UUID;

public class ProjectRepositoryTest {

    @NotNull
    private final ProjectRepository repository = new ProjectRepository();

    @Test
    public void add() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = new Project();
        project.setName("test1");
        project.setDescription("1111");
        project.setUserId("123456");
        repository.add(project);
        Assert.assertFalse(repository.findAll().isEmpty());
        Assert.assertNotNull(repository.findOneById(project.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.create("test", "DEMKA");
        Assert.assertFalse(repository.findAll(Sort.BY_NAME.getComparator()).isEmpty());
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create("test2", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.findOneById(""));
        Assert.assertNotNull(repository.findOneById(project.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));

        Assert.assertNull(repository.findOneById("", project.getId()));
        Assert.assertNull(repository.findOneById(null, project.getId()));
        Assert.assertNull(repository.findOneById(null, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(project.getUserId(), ""));
        Assert.assertNull(repository.findOneById(project.getUserId(), null));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), null));

        Assert.assertNotNull(repository.findOneById(project.getUserId(), project.getId()));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create("test2", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNotNull(repository.findOneByIndex(0));

        Assert.assertNull(repository.findOneByIndex(null, 0));
        Assert.assertNull(repository.findOneByIndex(project.getUserId(), null));
        Assert.assertNull(repository.findOneByIndex(UUID.randomUUID().toString(), null));

        Assert.assertNotNull(repository.findOneByIndex(project.getUserId(), 0));
    }

    @Test
    public void remove() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNotNull(repository.remove(project));

        Assert.assertNull(repository.remove(null));
        Assert.assertNull(repository.remove(null, null));
        Assert.assertNull(repository.remove(project.getUserId(), null));
        Assert.assertNull(repository.remove("12321", project));
        Assert.assertNull(repository.remove(UUID.randomUUID().toString(), project));
    }

    @Test
    public void clear() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(project.getUserId()));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));

        repository.clear();
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(0, repository.count(project.getUserId()));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
    }

    @Test
    public void removeById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        @Nullable Project testProject = repository.removeById(null);
        Assert.assertNull(testProject);
        Assert.assertEquals(1, repository.count());

        testProject = repository.removeById(null, null);
        Assert.assertNull(testProject);
        Assert.assertEquals(1, repository.count());

        testProject = repository.removeById(project.getUserId(), null);
        Assert.assertNull(testProject);
        Assert.assertEquals(1, repository.count());

        testProject = repository.removeById(project.getId());
        Assert.assertNotNull(testProject);
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void removeByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        @Nullable Project testProject = repository.removeByIndex(null);
        Assert.assertNull(testProject);
        Assert.assertEquals(1, repository.count());

        testProject = repository.removeByIndex(null, null);
        Assert.assertNull(testProject);
        Assert.assertEquals(1, repository.count());

        testProject = repository.removeByIndex(project.getUserId(), null);
        Assert.assertNull(testProject);
        Assert.assertEquals(1, repository.count());

        testProject = repository.removeByIndex(0);
        Assert.assertNotNull(testProject);
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void create() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create("test1", "DEMKA");
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById("123"));

        Assert.assertNull(repository.findOneById(null, null));
        Assert.assertNull(repository.findOneById(null, project.getId()));
        Assert.assertNull(repository.findOneById("1233", null));

        Assert.assertNotNull(repository.findOneById(project.getId()));
        Assert.assertNotNull(repository.findOneById(project.getUserId(), project.getId()));
    }
}
