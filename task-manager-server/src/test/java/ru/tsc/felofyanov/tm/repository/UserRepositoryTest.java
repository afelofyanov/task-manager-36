package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.model.User;

import java.util.UUID;

public class UserRepositoryTest {

    @NotNull
    private final UserRepository repository = new UserRepository();

    @Test
    public void add() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = new User();
        user.setLogin("123");
        user.setPasswordHash("123");
        repository.add(user);
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.findOneById(""));
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById("123"));
        Assert.assertNotNull(repository.findOneById(user.getId()));
    }

    @Test
    public void findByLogin() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = new User();
        user.setLogin("123");
        user.setPasswordHash("123");
        repository.add(user);
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.findByLogin(""));
        Assert.assertNull(repository.findByLogin(null));
        Assert.assertNull(repository.findByLogin(UUID.randomUUID().toString()));
        Assert.assertNotNull(repository.findByLogin(user.getLogin()));
    }

    @Test
    public void findByEmail() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = new User();
        user.setLogin("123");
        user.setEmail("login@login");
        repository.add(user);
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.findByEmail(""));
        Assert.assertNull(repository.findByEmail(null));
        Assert.assertNull(repository.findByEmail(UUID.randomUUID().toString()));
        Assert.assertNotNull(repository.findByEmail(user.getEmail()));
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = new User();
        user.setLogin("123");
        repository.add(user);
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertNull(repository.removeByLogin(""));
        Assert.assertNull(repository.removeByLogin(null));
        Assert.assertNull(repository.removeByLogin(UUID.randomUUID().toString()));
        Assert.assertNotNull(repository.removeByLogin(user.getLogin()));
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = new User();
        user.setLogin("123");
        repository.add(user);
        Assert.assertFalse(repository.findAll().isEmpty());

        Assert.assertFalse(repository.existsById(""));
        Assert.assertFalse(repository.existsById(null));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertTrue(repository.existsById(user.getId()));
    }
}
