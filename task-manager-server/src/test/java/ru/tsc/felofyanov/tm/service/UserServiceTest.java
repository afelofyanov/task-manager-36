package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;
import ru.tsc.felofyanov.tm.repository.TaskRepository;
import ru.tsc.felofyanov.tm.repository.UserRepository;

public class UserServiceTest {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final UserRepository userRepository = new UserRepository();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final UserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Test
    public void findByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));

        userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNull(userService.findByLogin("qwerty"));
        Assert.assertNotNull(userService.findByLogin("test"));
    }

    @Test
    public void findByEmail() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));

        userService.create("test", "test", "login@login");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNull(userService.findByEmail("qwerty"));
        Assert.assertNotNull(userService.findByEmail("login@login"));
    }

    @Test
    public void remove() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.remove(null));

        @NotNull User user = userService.create("test", "test");
        @Nullable User userTest = userService.findByEmail("qwerty");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertThrows(ModelNotFoundException.class, () -> userService.remove(userTest));
        Assert.assertNotNull(userService.remove(user));
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));

        @NotNull User user = userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNotNull(userService.removeByLogin(user.getLogin()));
    }

    @Test
    public void isLoginExists() {
        Assert.assertTrue(userService.findAll().isEmpty());

        @NotNull User user = userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertFalse(userService.isLoginExists("22222"));
        Assert.assertTrue(userService.isLoginExists(user.getLogin()));
    }


    @Test
    public void isEmailExists() {
        Assert.assertTrue(userService.findAll().isEmpty());

        @NotNull User user = userService.create("test", "test", "login@login");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertFalse(userService.isEmailExists(""));
        Assert.assertFalse(userService.isEmailExists("22222"));
        Assert.assertTrue(userService.isEmailExists(user.getEmail()));
    }

    @Test
    public void setPassword() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword("", null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("123", null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("123", ""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.setPassword("123", "234"));

        @NotNull User user = userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNotNull(userService.setPassword(user.getId(), "123"));
    }

    @Test
    public void updateUser() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.updateUser(null, "test", "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", null, "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", null, "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", "test", null));

        Assert.assertThrows(UserIdEmptyException.class, () -> userService.updateUser("", "test", "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "", "test", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", "", "test"));
        Assert.assertThrows(FIOEmptyException.class, () -> userService.updateUser("test", "test", "test", ""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.updateUser("123", "test", "test", "test"));

        @NotNull User user = userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertNotNull(userService.updateUser(user.getId(), "123", "asd", "qwerty"));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin("123"));

        @NotNull User user = userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        Assert.assertFalse(user.getLocked());

        userService.lockUserByLogin("test");
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin("123"));

        @NotNull User user = userService.create("test", "test");
        Assert.assertFalse(userService.findAll().isEmpty());

        user.setLocked(true);
        Assert.assertTrue(user.getLocked());

        userService.unlockUserByLogin("test");
        Assert.assertFalse(user.getLocked());
    }
}
