package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.*;
import ru.tsc.felofyanov.tm.api.repository.IDomainRepository;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.api.service.*;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.endpoint.*;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.repository.DomainRepository;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;
import ru.tsc.felofyanov.tm.repository.TaskRepository;
import ru.tsc.felofyanov.tm.repository.UserRepository;
import ru.tsc.felofyanov.tm.service.*;
import ru.tsc.felofyanov.tm.util.DateUtil;
import ru.tsc.felofyanov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainRepository domainRepository = new DomainRepository(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(domainRepository);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    public void registry(Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() {
        initPID();
        initData();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User test = userService.create("test", "test", Role.USUAL);

        taskService.add(test.getId(), new Task("DEMO TASK", Status.IN_PROGRESS, DateUtil.toDate("06.05.2019")));
        taskService.add(test.getId(), new Task("TEST TASK", Status.NOT_STARTED, DateUtil.toDate("03.01.2017")));
        taskService.add(test.getId(), new Task("MEGA TASK", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        taskService.add(test.getId(), new Task("BEST TASK", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));

        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED, DateUtil.toDate("05.06.2019")));
        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("03.01.2017")));
        projectService.add(test.getId(), new Project("MEGA PROJECT", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        projectService.add(test.getId(), new Project("BEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));
    }
}
