package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.api.service.ITaskService;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final Task task = create(userId, name, description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);

        @Nullable final Task task = findOneById(id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            final @Nullable String userId,
            final @Nullable Integer index,
            final @Nullable String name,
            final @Nullable String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);

        @Nullable final Task task = findOneByIndex(index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);

        @Nullable final Task task = findOneById(id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setUserId(userId);
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);

        @Nullable final Task task = findOneByIndex(index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setUserId(userId);
        task.setStatus(status);
        return task;
    }
}
