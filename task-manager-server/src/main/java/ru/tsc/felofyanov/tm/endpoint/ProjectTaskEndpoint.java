package ru.tsc.felofyanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.felofyanov.tm.api.service.IServiceLocator;
import ru.tsc.felofyanov.tm.dto.request.ProjectBindTaskByIdRequest;
import ru.tsc.felofyanov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.felofyanov.tm.dto.request.ProjectUnbindTaskByIdRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectBindTaskByIdResponse;
import ru.tsc.felofyanov.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.felofyanov.tm.dto.response.ProjectUnbindTaskByIdResponse;
import ru.tsc.felofyanov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint")
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @Nullable Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getServiceLocator().getProjectTaskService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectBindTaskByIdResponse bindTaskToProjectId(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectBindTaskByIdRequest request
    ) {
        @Nullable Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new ProjectBindTaskByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUnbindTaskByIdResponse unbindTaskFromProjectId(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUnbindTaskByIdRequest request
    ) {
        @Nullable Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new ProjectUnbindTaskByIdResponse();
    }
}
