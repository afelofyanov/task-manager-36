package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.model.AbstractUserOwnerModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel>
        extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(user -> userId.equals(user.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(user -> userId.equals(user.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return models.stream()
                .anyMatch(user -> userId.equals(user.getUserId()) && id.equals(user.getId()));
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(user -> id.equals(user.getId()) && userId.equals(user.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) return null;
        if (index == null) return null;
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(models::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @Nullable final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(models::remove);
        return model.orElse(null);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        return models.stream()
                .filter(user -> userId.equals(user.getUserId()))
                .count();
    }
}