package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnerService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd);

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);
}
