package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.AbstractUserOwnerModel;

import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnerModel> extends IUserOwnerRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);
}
